package com.nbsaas.boot.generate.ext.generator;


import com.nbsaas.boot.generate.ext.apis.Generator;

// 新增序列号生成器实现
public class KeyGenerator implements Generator {

    private String key;

    public KeyGenerator(String key) {
            this.key = key;
    }

    @Override
    public String generate() {
        return this.key;
    }
}