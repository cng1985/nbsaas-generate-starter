package com.nbsaas.boot.generate.ext.generator;


import com.nbsaas.boot.generate.ext.apis.Generator;

import java.util.Random;

// 随机生成器实现
public class RandomGenerator implements Generator {
    private int length;

    public RandomGenerator(int length) {
        this.length = length;
    }

    @Override
    public String generate() {
        Random random = new Random();
        StringBuilder randomPart = new StringBuilder();

        for (int i = 0; i < length; i++) {
            randomPart.append(random.nextInt(10));
        }

        return randomPart.toString();
    }
}