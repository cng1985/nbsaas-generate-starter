package com.nbsaas.boot.generate.ext.generator;

import com.nbsaas.boot.generate.ext.apis.Generator;
import com.nbsaas.boot.generate.ext.apis.GeneratorLong;

public class SnowflakeGenerator implements Generator {


    public SnowflakeGenerator(Snowflake snowflake) {
        this.snowflake = snowflake;
    }

    private Snowflake snowflake;

    @Override
    public String generate() {
        return snowflake.generate()+"";
    }
}
