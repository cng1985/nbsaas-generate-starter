package com.nbsaas.boot.generate.rest.convert;

import com.nbsaas.boot.generate.data.entity.GenerateSequence;
import com.nbsaas.boot.generate.api.domain.simple.GenerateSequenceSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class GenerateSequenceSimpleConvert implements Converter<GenerateSequenceSimple, GenerateSequence> {




@Override
public GenerateSequenceSimple convert(GenerateSequence source) {
    GenerateSequenceSimple result = new GenerateSequenceSimple();

                result.setStepKey(source.getStepKey());
                result.setCurStep(source.getCurStep());
                result.setStep(source.getStep());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}