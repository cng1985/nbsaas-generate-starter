package com.nbsaas.boot.generate.data.entity;

import com.nbsaas.boot.code.annotation.ComposeView;
import com.nbsaas.boot.code.annotation.CreateByUser;
import com.nbsaas.boot.code.annotation.FormAnnotation;
import com.nbsaas.boot.code.annotation.FormField;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import lombok.Data;
import org.hibernate.annotations.Comment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "common_no_sequence")
@org.hibernate.annotations.Table(appliesTo = "common_no_sequence", comment = "步长生成器")
@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "步长生成器", model = "订单")
public class GenerateSequence extends LongEntity {

    @Column(length = 50, unique = true)
    @Comment("生成器key")
    private String stepKey;

    @Comment("步长")
    private Integer step;

    @Comment("当前值")
    private Long curStep;

    @FormField(title = "创建时间", grid = true, width = "160", ignore = true)
    @Comment("创建时间")
    private Date addDate;

    @Comment("最新修改时间")
    private Date lastDate;

}
