package com.nbsaas.boot.generate.ext.resource;

import com.nbsaas.boot.generate.api.apis.GenerateSequenceApi;
import com.nbsaas.boot.generate.ext.apis.GenerateSequenceExtApi;
import com.nbsaas.boot.generate.ext.generator.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class GenerateSequenceExtResource implements GenerateSequenceExtApi {

    @Resource
    private GenerateSequenceApi generateSequenceApi;


    @Override
    public String generate(String key) {



        CompositeGenerator generator = new CompositeGenerator(
                new KeyGenerator(key),
                new DateGenerator("yyyyMMdd"),
                new RandomGenerator(4),
                new StepGenerator(key,generateSequenceApi,6)
        );
        return generator.generate();
    }

    @Transactional
    @Override
    public String generateKey(String key) {
        return generate(key);
    }
}
