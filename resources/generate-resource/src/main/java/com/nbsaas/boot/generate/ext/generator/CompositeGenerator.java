package com.nbsaas.boot.generate.ext.generator;


import com.nbsaas.boot.generate.ext.apis.Generator;

// 组合生成器
public class CompositeGenerator implements Generator {
    private Generator[] generators;

    public CompositeGenerator(Generator... generators) {
        this.generators = generators;
    }

    @Override
    public String generate() {
        StringBuilder result = new StringBuilder();
        for (Generator generator : generators) {
            result.append(generator.generate());
        }
        return result.toString();
    }
}
