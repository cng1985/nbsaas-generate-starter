package com.nbsaas.boot.generate.rest.convert;

import com.nbsaas.boot.generate.data.entity.GenerateSequence;
import com.nbsaas.boot.generate.api.domain.response.GenerateSequenceResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class GenerateSequenceResponseConvert  implements Converter<GenerateSequenceResponse,GenerateSequence> {

    @Override
    public GenerateSequenceResponse convert(GenerateSequence source) {
        GenerateSequenceResponse  result = new  GenerateSequenceResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

