package com.nbsaas.boot.generate.ext.generator;


import com.nbsaas.boot.generate.ext.apis.Generator;

import java.text.SimpleDateFormat;
import java.util.Date;
// 日期生成器实现
public class DateGenerator implements Generator {
    private SimpleDateFormat dateFormat;

    public DateGenerator(String dateFormatPattern) {
        this.dateFormat = new SimpleDateFormat(dateFormatPattern);
    }

    @Override
    public String generate() {
        return dateFormat.format(new Date());
    }
}
