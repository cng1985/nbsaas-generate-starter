package com.nbsaas.boot.generate.ext.generator;

import com.nbsaas.boot.generate.api.apis.GenerateSequenceApi;
import com.nbsaas.boot.generate.api.domain.field.GenerateSequenceField;
import com.nbsaas.boot.generate.api.domain.request.GenerateSequenceRequest;
import com.nbsaas.boot.generate.api.domain.response.GenerateSequenceResponse;
import com.nbsaas.boot.generate.ext.apis.Generator;
import com.nbsaas.boot.rest.filter.Filter;

import java.util.Date;

// 步长生成器实现
public class StepGenerator implements Generator {
    private int stepLength;

    private String stepKey;

    private GenerateSequenceApi noSequenceApi;

    public StepGenerator(String stepKey, GenerateSequenceApi noSequenceApi, int stepLength) {
        this.stepLength = stepLength;
        this.noSequenceApi = noSequenceApi;
        this.stepKey = stepKey;

    }

    @Override
    public String generate() {
        GenerateSequenceResponse no = noSequenceApi.oneData(Filter.eq(GenerateSequenceField.stepKey, stepKey));
        if (no==null){
            GenerateSequenceRequest req=new GenerateSequenceRequest();
            req.setStep(1);
            req.setStepKey(stepKey);
            req.setCurStep(0L);
            req.setAddDate(new Date());
            req.setLastDate(new Date());
            no = noSequenceApi.createData(req);
        }
        Long currentStep=no.getCurStep()+no.getStep();
        String stepPart = String.format("%0" + stepLength + "d", currentStep);

        GenerateSequenceRequest request=new GenerateSequenceRequest();
        request.setId(no.getId());
        request.setCurStep(currentStep);
        request.setLastDate(new Date());
        noSequenceApi.update(request);
        return stepPart;
    }
}