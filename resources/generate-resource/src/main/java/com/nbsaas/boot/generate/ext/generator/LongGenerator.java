package com.nbsaas.boot.generate.ext.generator;


import com.nbsaas.boot.generate.ext.apis.Generator;

public class LongGenerator implements Generator {

    private Long value;

    public LongGenerator(Long value) {
        this.value = value;
    }

    @Override
    public String generate() {
        String stepPart = String.format("%0" + 10 + "d", value);

        return stepPart;
    }

    public static void main(String[] args) {

        for (int i = 0; i < 100000; i++) {
            LongGenerator generator = new LongGenerator(1000L + i);
            //System.out.println(generator.generate());
        }
    }
}
