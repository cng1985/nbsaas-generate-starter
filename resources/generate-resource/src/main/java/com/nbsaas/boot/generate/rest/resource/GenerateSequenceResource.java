package com.nbsaas.boot.generate.rest.resource;

import com.nbsaas.boot.generate.api.apis.GenerateSequenceApi;
import com.nbsaas.boot.generate.data.entity.GenerateSequence;
import com.nbsaas.boot.generate.api.domain.request.GenerateSequenceRequest;
import com.nbsaas.boot.generate.api.domain.response.GenerateSequenceResponse;
import com.nbsaas.boot.generate.api.domain.simple.GenerateSequenceSimple;
import com.nbsaas.boot.generate.rest.convert.GenerateSequenceSimpleConvert;
import com.nbsaas.boot.generate.rest.convert.GenerateSequenceEntityConvert;
import com.nbsaas.boot.generate.rest.convert.GenerateSequenceResponseConvert;
import com.nbsaas.boot.generate.data.repository.GenerateSequenceRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
/**
*   业务接口实现
*/
@Transactional
@Service
public class GenerateSequenceResource extends BaseResource<GenerateSequence,GenerateSequenceResponse, GenerateSequenceSimple, GenerateSequenceRequest>  implements GenerateSequenceApi {

    @Resource
    private GenerateSequenceRepository generateSequenceRepository;

    @Override
    public JpaRepositoryImplementation<GenerateSequence, Serializable> getJpaRepository() {
        return generateSequenceRepository;
    }

    @Override
    public Function<GenerateSequence, GenerateSequenceSimple> getConvertSimple() {
        return new GenerateSequenceSimpleConvert();
    }

    @Override
    public Function<GenerateSequenceRequest, GenerateSequence> getConvertForm() {
        return new GenerateSequenceEntityConvert();
    }

    @Override
    public Function<GenerateSequence, GenerateSequenceResponse> getConvertResponse() {
        return new GenerateSequenceResponseConvert();
    }




}


