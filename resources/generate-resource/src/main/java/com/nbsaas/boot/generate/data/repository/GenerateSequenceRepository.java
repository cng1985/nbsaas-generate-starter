package com.nbsaas.boot.generate.data.repository;

import com.nbsaas.boot.generate.data.entity.GenerateSequence;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface GenerateSequenceRepository  extends  JpaRepositoryImplementation<GenerateSequence, Serializable>{

}