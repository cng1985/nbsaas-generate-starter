package com.nbsaas.boot.generate.rest.convert;

import com.nbsaas.boot.generate.data.entity.GenerateSequence;
import com.nbsaas.boot.generate.api.domain.request.GenerateSequenceRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class GenerateSequenceEntityConvert  implements Converter<GenerateSequence, GenerateSequenceRequest> {

    @Override
    public GenerateSequence convert(GenerateSequenceRequest source) {
        GenerateSequence result = new GenerateSequence();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

