package com.nbsaas.boot.generate.ext.apis;

public interface GenerateSequenceExtApi {

    String generate(String key);


    String generateKey(String key);

}
