package com.nbsaas.boot.generate.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
/**
* 请求对象
*/
@Data
public class GenerateSequenceRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 生成器key
        **/
            private String stepKey;

        /**
        * 当前值
        **/
            private Long curStep;

        /**
        * 步长
        **/
            private Integer step;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 创建时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}