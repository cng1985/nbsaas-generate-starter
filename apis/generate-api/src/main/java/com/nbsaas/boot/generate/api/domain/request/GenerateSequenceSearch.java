package com.nbsaas.boot.generate.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GenerateSequenceSearch   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



            /**
            * 生成器key
            **/
            @Search(name = "stepKey",operator = Operator.like)
            private String stepKey;

            /**
            * 当前值
            **/
            @Search(name = "curStep",operator = Operator.eq)
            private Long curStep;

            /**
            * 步长
            **/
            @Search(name = "step",operator = Operator.eq)
            private Integer step;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;



}