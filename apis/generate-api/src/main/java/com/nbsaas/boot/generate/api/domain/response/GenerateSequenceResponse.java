package com.nbsaas.boot.generate.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class GenerateSequenceResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 生成器key
        **/
            private String stepKey;

        /**
        * 当前值
        **/
            private Long curStep;

        /**
        * 步长
        **/
            private Integer step;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 创建时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}