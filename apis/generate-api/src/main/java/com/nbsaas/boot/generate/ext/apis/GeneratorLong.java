package com.nbsaas.boot.generate.ext.apis;

public interface GeneratorLong {

    Long generate();

}
