package com.nbsaas.boot.generate.ext.apis;

/**
 * 编号生成器
 */
public interface Generator {

    /**
     * 生成编号
     *
     * @return
     */
    String generate();
}