package com.nbsaas.boot.generate.api.apis;

import com.nbsaas.boot.generate.api.domain.request.GenerateSequenceRequest;
import com.nbsaas.boot.generate.api.domain.simple.GenerateSequenceSimple;
import com.nbsaas.boot.generate.api.domain.response.GenerateSequenceResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface GenerateSequenceApi extends BaseApi<GenerateSequenceResponse, GenerateSequenceSimple, GenerateSequenceRequest> {


}
