package com.nbsaas.boot.generate.api.domain.field;


/**
*   字段映射类
*/
public class GenerateSequenceField  {



    /**
    * 生成器key
    **/
    public static final String  stepKey = "stepKey";


    /**
    * 当前值
    **/
    public static final String  curStep = "curStep";


    /**
    * 步长
    **/
    public static final String  step = "step";


    /**
    * 主键id
    **/
    public static final String  id = "id";


    /**
    * 创建时间
    **/
    public static final String  addDate = "addDate";


    /**
    * 最新修改时间
    **/
    public static final String  lastDate = "lastDate";

}